%%%-------------------------------------------------------------------
%%% @author Chaitanya Chalasani
%%% @copyright (C) 2021, ArkNode.IO
%%% @doc
%%%
%%% @end
%%% Created : 2021-06-27 14:12:51.952969
%%%-------------------------------------------------------------------
-module(knowhere).

-behaviour(gen_server).

-define(MAX_STACK, 100).
-define(TOPIC_EXPIRY, 36000).

%% API
-export([start_link/0
        ,subscribe/1
        ,subscribe/2
        ,unsubscribe/1
        ,unsubscribe/2
        ,publish/2
        ,publish/3
        ,purge/0
        ,purge/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

subscribe(Topic) ->
  subscribe(Topic, self()).

subscribe(Topic, Pid) ->
  gen_server:cast(?MODULE, {subscribe, Topic, Pid}).

unsubscribe(Topic) ->
  unsubscribe(Topic, self()).

unsubscribe(Topic, Pid) ->
  gen_server:cast(?MODULE, {unsubscribe, Topic, Pid}).

publish(Topic, Event) ->
  publish(Topic, Event, #{}).

publish(Topic, Event, Options) when is_binary(Topic), is_map(Options) ->
  gen_server:cast(?MODULE, {publish, Topic, Event, Options}).

purge() -> purge(#{}).

purge(Options) when is_map(Options) ->
  gen_server:cast(?MODULE, {purge, Options}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->
  {ok, #{}}.

handle_call(_Request, _From, State) ->
  Reply = ok,
  {reply, Reply, State}.

handle_cast({subscribe, Topic, Pid}, State) ->
  do_subscribe(Topic, Pid),
  {noreply, State};
handle_cast({unsubscribe, Topic, Pid}, State) ->
  do_unsubscribe(Topic, Pid),
  {noreply, State};
handle_cast({publish, Topic, Event, Options}, State) ->
  do_publish(Topic, Event, Options),
  {noreply, State};
handle_cast({purge, Options}, State) ->
  do_purge(Options),
  {noreply, State};
handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info({{'DOWN', Topic}, _, process, Pid, _}, State) ->
  do_unsubscribe(Topic, Pid),
  {noreply, State};
handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

do_subscribe(Topic, Pid) ->
  case knowhere_tivan:get(#{topic => Topic}) of
    [] ->
      knowhere_tivan:put(#{topic => Topic, subscribers => [Pid]});
    [#{subscribers := Pids}] ->
      PidsU = lists:usort([Pid|Pids]),
      knowhere_tivan:update(#{topic => Topic}, #{subscribers => PidsU}),
      send_prev_events(Topic, Pid),
      erlang:monitor(process, Pid, [{tag, {'DOWN', Topic}}])
  end.

do_unsubscribe(Topic, Pid) ->
  case knowhere_tivan:get(#{topic => Topic}) of
    [] -> ignore;
    [#{subscribers := Pids, event := Event}] ->
      case lists:delete(Pid, Pids) of
        [] when Event == undefined ->
          knowhere_tivan:delete(#{topic => Topic});
        PidsU ->
          knowhere_tivan:update(#{topic => Topic}, #{subscribers => PidsU})
      end
  end.

do_publish(Topic, Event, Options) ->
  case knowhere_tivan:get(#{topic => Topic}) of
    [] ->
      knowhere_tivan:put(#{topic => Topic, event => Event});
    [#{subscribers := Pids, event := EventPrev, events := EventStack}] ->
      Update = case maps:get(stack, Options, false) of
                 true ->
                   MaxStack = maps:get(max_stack
                                       ,Options
                                       ,application:get_env(knowhere, max_stack, ?MAX_STACK)),
                   EventStackU = lists:sublist([EventPrev|EventStack], MaxStack),
                   #{event => Event, events => EventStackU};
                 _ ->
                   #{event => Event, events => []}
               end,
      knowhere_tivan:update(#{topic => Topic}, Update),
      send_event(Pids, Topic, Event)
  end,
  publish_parent_topics(Topic, Event).

publish_parent_topics(Topic, Event) ->
  lists:foreach(
    fun(Parent) ->
        case knowhere_tivan:get(#{topic => Parent}) of
          [] -> ignore;
          [#{subscribers := Pids}] ->
            send_event(Pids, Topic, Event)
        end
    end,
    parent_topics(Topic)
   ).

parent_topics(Topic) ->
  case string:split(Topic, ".", trailing) of
    [Topic] -> [];
    [Parent|_] -> [Parent|parent_topics(Parent)]
  end.

send_prev_events(Topic, Pid) ->
  lists:foreach(
    fun(#{event := undefined}) -> ignore;
       (#{topic := T, event := E, events := Es}) ->
        send_event(Pid, T, [E|Es])
    end,
    knowhere_tivan:get(#{topic => <<Topic/binary, "...">>})
   ).

send_event(Pids, Topic, Event) when is_list(Pids) ->
  lists:foreach(fun(Pid) -> send_event(Pid, Topic, Event) end, Pids);
send_event(Pid, Topic, Events) when is_list(Events) ->
  lists:foreach(fun(Event) -> send_event(Pid, Topic, Event) end, Events);
send_event(Pid, Topic, Event) -> (catch erlang:send(Pid, {knowhere, Topic, Event})).

do_purge(Options) ->
  TopicExpiry = maps:get(topic_expiry, Options
                         ,application:get_env(knowhere, topic_expiry, ?TOPIC_EXPIRY)) * 1000,
  Now = erlang:system_time(millisecond),
  lists:foreach(
    fun(#{a_mtime := AMTime} = Topic) when AMTime + TopicExpiry < Now ->
        knowhere_tivan:delete(Topic);
       (_) -> ignore
    end,
    knowhere_tivan:get()
   ).

