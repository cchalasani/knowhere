-module(knowhere_tivan).

-behaviour(tivan_server).

-export([start_link/0
        ,put/1
        ,get/0
        ,get/1
        ,delete/1
        ,update/2]).

-export([init/1]).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
  tivan_server:start_link({local , ?MODULE}, ?MODULE, [], []).

put(Topic) when is_map(Topic) ->
  tivan_server:put(?MODULE, knowhere, Topic).

get() ->
  tivan_server:get(?MODULE, knowhere, #{}).

get(Options) when is_map(Options) ->
  tivan_server:get(?MODULE, knowhere, Options).

delete(Topic) when is_binary(Topic) ->
  delete(#{topic => Topic});
delete(Topic) when is_map(Topic) ->
  tivan_server:remove(?MODULE, knowhere, Topic).

update(Options, Updates) when is_map(Options), is_map(Updates) ->
  tivan_server:update(?MODULE, knowhere, Options, Updates).

%%%===================================================================
%%% tivan_server callbacks
%%%===================================================================

init([]) ->
  TableDefs = #{
    knowhere => #{columns => #{topic => #{type => binary
                                         ,key => true}
                              ,subscribers => #{type => [pid]
                                               ,default => []}
                              ,event => #{type => term}
                              ,events => #{type => list
                                          ,default => []}}
                 ,audit => true
                 ,persist => false
                 ,type => ordered_set}
   },
  {ok, TableDefs}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

